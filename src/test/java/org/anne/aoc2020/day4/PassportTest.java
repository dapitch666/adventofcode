package org.anne.aoc2020.day4;

import static org.junit.Assert.*;

import org.junit.Test;

public class PassportTest {
    @Test
    public void testValidHeight() {
        assertTrue(validHgt("59in"));
        assertTrue(validHgt("190cm"));
        assertFalse(validHgt("77in"));
        assertFalse(validHgt("190"));
        assertFalse(validHgt("Tot190cm"));
        assertFalse(validHgt(""));
    }

    @Test
    public void testValidHairColor() {
        assertTrue(validHcl("#123abc"));
        assertFalse(validHcl("#123abz"));
        assertFalse(validHcl("123abc"));
        assertFalse(validHcl(""));
    }

    private boolean validHgt(String height) {
        String pattern = "^(\\d+)(in|cm)$";
        if (height.matches(pattern)) {
            int heightValue = Integer.parseInt(height.replaceAll(pattern, "$1"));
            String heightUnit = height.replaceAll(pattern, "$2");
            if (heightUnit.equals("in")) {
                return heightValue >= 59 && heightValue <= 76;
            } else {
                return heightValue >= 150 && heightValue <= 193;
            }
        }
        return false;
    }

    private boolean validHcl(String hairColor) {
        return hairColor.matches("^#[0-9a-f]{6}$");
    }
}