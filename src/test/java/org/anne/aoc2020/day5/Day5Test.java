package org.anne.aoc2020.day5;

import org.junit.Test;

import static org.junit.Assert.*;

public class Day5Test {

    @Test
    public void testDecodeRow() {
        assertEquals(44, decodeTicket("FBFBBFFRLR", true));
        assertEquals(70, decodeTicket("BFFFBBFRRR", true));
        assertEquals(14, decodeTicket("FFFBBBFRRR", true));
        assertEquals(102, decodeTicket("BBFFBBFRLL", true));
    }

    @Test
    public void testDecodeColumn() {
        assertEquals(5, decodeTicket("FBFBBFFRLR", false));
        assertEquals(7, decodeTicket("BFFFBBFRRR", false));
        assertEquals(7, decodeTicket("FFFBBBFRRR", false));
        assertEquals(4, decodeTicket("BBFFBBFRLL", false));
    }

    private int decodeTicket(String ticket, boolean isRow) {
        int min = 0;
        int max;
        char up;
        char down;
        if (isRow) {
            max = 127;
            up = 'F';
            down = 'B';
        } else {
            max = 7;
            up = 'L';
            down = 'R';
        }
        for (char c : ticket.toCharArray()) {
            int boundary =  ((max - min + 1) / 2) + min;
            if (c == up) {
                max = boundary - 1;
            } else if (c == down){
                min = boundary;
            }
        }
        return min;
    }
}