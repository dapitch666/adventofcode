package org.anne.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class File {

    public static List<String> readFile(String filename){
        String filePath = "src/main/resources/aoc2020/" + filename;
        try {
            return Files.readAllLines(Paths.get(filePath));
        } catch (IOException e) {
            System.err.println("There was an Error reading the File.");
            return new ArrayList<>();
        }
    }

}
