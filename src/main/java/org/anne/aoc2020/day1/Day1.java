package org.anne.aoc2020.day1;

import org.anne.util.File;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Day1 {
    private static final String fileName = "Day1.txt";

    public static void main(String[] args) {
        new Day1();
    }

    public Day1() {
        List<String> input = File.readFile(fileName);
        System.out.println("Défi 1: " + defi1(input));
        System.out.println("Défi 2: " + defi2(input));
    }

    private String defi1(List<String> input) {
        List<Integer> entryList = input.stream().map(Integer::parseInt).collect(Collectors.toList());
        while (entryList.size() > 0) {
            int entry = entryList.get(0);
            entryList.remove(0);
            for (int i : entryList) {
                if (entry + i == 2020) {
                    return entry + " x " + i + " = " + entry * i;
                }
            }
        }
        return "No solution";
    }

    private String defi2(List<String> input) {
        List<Integer> entryList = input.stream().map(Integer::parseInt).collect(Collectors.toList());
        while (entryList.size() > 0) {
            int first = entryList.get(0);
            entryList.remove(0);
            List<Integer> entryListCopy = new ArrayList<>(entryList);
            while (entryListCopy.size() > 0) {
                int second = entryListCopy.get(0);
                entryListCopy.remove(0);
                int somme = first + second;
                if (somme < 2020) {
                    for (int i : entryListCopy) {
                        if (somme + i == 2020) {
                            return first + " x " + second + " x " + i + " = " + first * second * i;
                        }
                    }
                }
            }
        }
        return "No solution";
    }
}
