package org.anne.aoc2020.day12;

import org.anne.util.File;

import java.util.Arrays;
import java.util.List;

public class Day12 {
    private static final String fileName = "Day12.txt";
    char[] cardinal = {'N', 'E', 'S', 'W'};

    public static void main(String[] args) {
        new Day12();
    }

    public Day12() {
        List<String> input = File.readFile(fileName);

        System.out.println("Part1 Manhattan distance = " + part1(input));
        System.out.println("Part1 Manhattan distance = " + part2(input));
    }

    private int part2(List<String> input) {
        int x = 0, y = 0;
        int waypointX = 10, waypointY = 1;
        for (String s : input) {
            char instruction = s.charAt(0);
            if (instruction == 'R' || instruction == 'L') {
                int degree = Integer.parseInt(s.substring(1));
                int nbRotation = instruction == 'R' ? degree / 90 : (360 - degree) / 90;
                int newX = 0, newY = 0;
                switch (nbRotation) {
                    case 1:
                        newX = waypointY;
                        newY = -waypointX;
                        break;
                    case 2:
                        newX = -waypointX;
                        newY = -waypointY;
                        break;
                    case 3:
                        newX = -waypointY;
                        newY = waypointX;
                }
                waypointX = newX;
                waypointY = newY;
            } else if (instruction == 'F') {
                int mult = Integer.parseInt(s.substring(1));
                x = x + mult * waypointX;
                y = y + mult * waypointY;
            } else {
                int distance = Integer.parseInt(s.substring(1));
                int[] direction = getDir(instruction);
                waypointX = waypointX + distance * direction[0];
                waypointY = waypointY + distance * direction[1];
            }
        }
        // System.out.println("x: " + x + " y: " + y + " waypontX: " + waypointX + " waypointY: " + waypointY);
        return Math.abs(x) + Math.abs(y);
    }

    private int part1(List<String> input) {
        int x = 0, y = 0;
        char facing = 'E';
        for (String s : input) {
            char instruction = s.charAt(0);
            if (instruction == 'R' || instruction == 'L') {
                facing = rotate(facing, instruction, Integer.parseInt(s.substring(1)));
            } else {
                if (instruction == 'F') {
                    instruction = facing;
                }
                if (isCardinal(instruction)) {
                    int[] direction = getDir(instruction);
                    int distance = Integer.parseInt(s.substring(1));
                    x = x + distance * direction[0];
                    y = y + distance * direction[1];
                }
            }
        }
        return Math.abs(x) + Math.abs(y);
    }

    private char rotate(char facing, char instruction, int degree) {
        int nbRotation = instruction == 'R' ? degree / 90 : (360 - degree) / 90;
        int curIdx = new String(cardinal).indexOf(facing);
        if (curIdx + nbRotation < cardinal.length) {
            return cardinal[curIdx + nbRotation];
        } else {
            return cardinal[curIdx + nbRotation - cardinal.length];
        }
    }

    private boolean isCardinal(char instruction) {
        return new String(cardinal).indexOf(instruction) >= 0;
    }

    public static int[] getDir (char c) {
        switch (c) {
            case 'N': return new int[] {0,1};
            case 'S': return new int[] {0,-1};
            case 'W': return new int[] {-1,0};
            case 'E': return new int[] {1,0};
        }
        return null;
    }


}
