package org.anne.aoc2020.day14;

import org.anne.util.File;

import java.util.*;

public class Day14 {
    private static final String fileName = "Day14.txt";

    public static void main(String[] args) {
        new Day14();
    }

    public Day14() {
        List<String> input = File.readFile(fileName);

        System.out.println("The sum of all values left in memory for part1 is " + part1(input));
        System.out.println("The sum of all values left in memory for part2 is " + part2(input));
    }

    private long part1(List<String> input) {
        char[] mask = new char[36];
        Map<Integer, Long> memoryMap = new HashMap<>();
        for (String line : input) {
            String[] l = line.split(" = ");
            if (l[0].equals("mask")) {
                mask = l[1].toCharArray();
            } else {
                int memIdx = Integer.parseInt(l[0].replaceAll("mem\\[(\\d+)]", "$1"));
                char[] memory = longToBinaryStringLeadingZeros(Long.parseLong(l[1]));
                for (int i = 0; i < 36; i++) {
                    if (mask[i] != 'X') {
                        memory[i] = mask[i];
                    }
                }
                memoryMap.put(memIdx, Long.parseLong(String.valueOf(memory), 2));
            }
        }
        return memoryMap.values().stream().mapToLong(Long::longValue).sum();
    }

    private long part2(List<String> input) {
        char[] mask = new char[36];
        Map<Long, Long> memoryMap = new HashMap<>();
        for (String line : input) {
            String[] l = line.split(" = ");
            if (l[0].equals("mask")) {
                mask = l[1].toCharArray();
            } else {
                long memIdx = Long.parseLong(l[0].replaceAll("mem\\[(\\d+)]", "$1"));
                long value = Long.parseLong(l[1]);

                char[] binaryString = longToBinaryStringLeadingZeros(memIdx);

                Set<Long> mems = getMems(binaryString, 0, mask);
                for (long key : mems) {
                    memoryMap.put(key, value);
                }
            }
        }
        return memoryMap.values().stream().mapToLong(Long::longValue).sum();
    }

    private char[] longToBinaryStringLeadingZeros(long val) {
        String binaryString = Long.toBinaryString(val);
        return ("000000000000000000000000000000000000" + binaryString).substring(binaryString.length()).toCharArray();
    }

    static Set<Long> getMems(char[] mem, int i, char[] mask) {
        Set<Long> set = new HashSet<>();
        if (i == 36) {
            set.add(Long.parseLong(String.valueOf(mem), 2));
            return set;
        }
        switch (mask[i]) {
            case '0':
                set.addAll(getMems(mem, i+1, mask));
                break;
            case '1':
                mem[i] = '1';
                set.addAll(getMems(mem, i+1, mask));
                break;
            default:
                char[] memA = mem.clone(), memB = mem.clone();
                memA[i] = '1';
                memB[i] = '0';
                set.addAll(getMems(memA, i+1, mask));
                set.addAll(getMems(memB, i+1, mask));
        }
        return set;
    }
}
