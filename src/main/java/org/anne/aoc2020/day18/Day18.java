package org.anne.aoc2020.day18;

import org.anne.util.File;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Day18 {
    private static final String fileName = "Day18.txt";
    private static boolean isPart2 = false;

    public static void main(String[] args) {
        new Day18();
    }

    public Day18() {
        List<String> input = File.readFile(fileName);

        System.out.println("The sum of all the result of Part1 is " + input.stream().mapToLong(this::calculate).sum());
        isPart2 = true;
        System.out.println("The sum of all the result of Part2 is " + input.stream().mapToLong(this::calculate).sum());
    }

    private long calculate(String operation) {

        while (operation.contains("(")) {
            int start = operation.lastIndexOf("(");
            int end = operation.indexOf(")", start);
            long result = calculate(operation.substring(start + 1, end));
            operation = operation.substring(0, start) + result + operation.substring(end + 1);
        }

        long left = 0L;
        long right = 0L;
        boolean isAddition = true;
        List<String> op = new ArrayList<>(Arrays.asList(operation.split(" ")));
        if (isPart2) {
            while (op.contains("+")) {
                int plusIndex = op.indexOf("+");
                long result = Long.parseLong(op.get(plusIndex - 1)) + Long.parseLong(op.get(plusIndex + 1));
                op.set(plusIndex, String.valueOf(result));
                op.remove(plusIndex + 1);
                op.remove(plusIndex - 1);
            }
        }

        for (String c : op) {
            if (c.equals("+")) {
                isAddition = true;
            } else if (c.equals("*")) {
                isAddition = false;
            } else {
                if (left == 0) {
                    left = Long.parseLong(c);
                } else {
                    right = Long.parseLong(c);
                }
            }
            if (right != 0L) {
                if (isAddition) {
                    left += right;
                } else {
                    left *= right;
                }
                right = 0L;
            }
        }
        return left;
    }
}
