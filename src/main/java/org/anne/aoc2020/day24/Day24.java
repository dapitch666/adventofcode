package org.anne.aoc2020.day24;

import org.anne.util.File;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Day24 {
    private static final String fileName = "Day24.txt";
    private static final int DAYS = 100;

    public static void main(String[] args) {
        new Day24();
    }

    public Day24() {
        List<String> input = File.readFile(fileName);
        Map<String, Integer> floor = new HashMap<>();
        for (String line : input) {
            int x = 0;
            int y = 0;
            boolean prev = false;
            for (char dir : line.toCharArray()) {
                if (dir == 'e') {
                    if (prev) {
                        x += 1;
                    } else {
                        x += 2;
                    }
                    prev = false;
                } else if (dir == 'w') {
                    if (prev) {
                        x -= 1;
                    } else {
                        x -= 2;
                    }
                    prev = false;
                } else {
                    prev = true;
                    if (dir == 'n') {
                        y += 1;
                    } else {
                        y -= 1;
                    }
                }
            }
            HexTile tile = new HexTile(x, y);
            int count = floor.getOrDefault(tile.toString(), 0);
            floor.put(tile.toString(), count + 1);
        }
        int size = floor.keySet().stream().mapToInt(s -> Integer.parseInt(s.split(", ")[0].replaceAll("x: ", ""))).max().orElse(-1) + 1 + DAYS;

        int[][] flooor = new int[size * 2][size * 2];
        for (Map.Entry<String, Integer> entry : floor.entrySet()) {
            HexTile tile = new HexTile(entry.getKey());
            if (entry.getValue() % 2 != 0) {
                flooor[size + tile.x][size + tile.y] = 1;
            }
        }
        System.out.println("Part1: " + Arrays.stream(flooor).flatMapToInt(Arrays::stream).sum() + " tiles are black");

        for (int i = 0; i < DAYS; i++) {
            int[][] flooorCopy = new int[size * 2][size * 2];
            for (int x = 0; x < size * 2; x++) {
                for (int y = (x%2 == 0) ? 0 : 1 ; y < size * 2; y += 2) {
                    int activeNeighbors = countActiveNeighbors(flooor, x, y);
                    if (flooor[x][y] == 1) {
                        if (activeNeighbors == 1 || activeNeighbors == 2) {
                            flooorCopy[x][y] = 1;
                        }
                    } else if (activeNeighbors == 2) {
                        flooorCopy[x][y] = 1;
                    }
                }
            }
            flooor = flooorCopy;
        }
        System.out.println("Part2: " + Arrays.stream(flooor).flatMapToInt(Arrays::stream).sum() + " tiles are black");
    }


    private int countActiveNeighbors(int[][] flooor, int x, int y) {
        int count = 0;
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                if (i == 0 && j == 0) {
                    continue;
                }
                try {
                    count += flooor[x+i][y+j];
                } catch (ArrayIndexOutOfBoundsException e) {
                    // Do nothing
                }
            }
        }
        try {
            count += flooor[x - 2][y];
        } catch (ArrayIndexOutOfBoundsException e) {
            // Do nothing
        }
        try {
            count += flooor[x + 2][y];
        } catch (ArrayIndexOutOfBoundsException e) {
            // Do nothing
        }
        return count;
    }


    private static class HexTile {
        private final int x;
        private final int y;

        private HexTile(int x, int y) {
            this.x = x;
            this.y = y;
        }

        private HexTile(String str) {
            String[] coord = str.split(", ");
            this.x = Integer.parseInt(coord[0].replaceAll("x: ", ""));
            this.y = Integer.parseInt(coord[1].replaceAll("y: ", ""));
        }

        public String toString() {
            return "x: " + this.x + ", y: " + this.y;
        }

        public boolean equals(Object obj) {
            if(!(obj instanceof HexTile)) {
                return false;
            }

            HexTile tile = (HexTile) obj;
            return tile.x == x && tile.y == y;
        }
    }
}
