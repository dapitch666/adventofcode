package org.anne.aoc2020.day20;

import java.util.*;

public class Tile {
    private final int id;
    private int[][] image;
    private final int size;
    private int top = 0;
    private int right = 0;
    private int bottom = 0;
    private int left = 0;

    public int[] topBorder() {
        return image[0];
    }

    public int[] bottomBorder() {
        return image[size - 1];
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public int getRight() {
        return right;
    }

    public void setRight(int right) {
        this.right = right;
    }

    public int getBottom() {
        return bottom;
    }

    public void setBottom(int bottom) {
        this.bottom = bottom;
    }

    public int getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    public Tile(int id, List<String> input) {
        this.id = id;
        this.size = input.get(0).length();
        this.image = newTile();
        for (int i = 0; i < this.size; i++) {
            for (int j = 0; j < this.size; j++) {
                if (input.get(i).charAt(j) == '#')
                    this.image[i][j] = 1;
            }
        }
    }

    public Tile(int[][] image) {
        this.id = 0;
        this.image = image;
        this.size = image[0].length;
    }

    public int getId() {
        return id;
    }

    public int[][] getImage() {
        return image;
    }

    public int getNbNeighbours() {
        int count = 0;
        if (top != 0) count++;
        if (right != 0) count++;
        if (bottom != 0) count++;
        if (left != 0) count++;
        return count;
    }

    public boolean isCorner() {
        return getNbNeighbours() == 2;
    }

    private int[][] newTile() {
        int[][] tile = new int[this.size][this.size];
        for (int i = 0; i < this.size; i++) {
            for (int j = 0; j < this.size; j++) {
                tile[i][j] = 0;
            }
        }
        return tile;
    }

    public int[][] getBorderlessTile() {
        int[][] tile = new int[this.size - 2][this.size - 2];
        for (int i = 0; i < this.size - 2; i++) {
            for (int j = 0; j < this.size - 2; j++) {
                tile[i][j] = this.image[i+1][j+1];
            }
        }
        return tile;
    }

    public void rotate() {
            int[][] ret = new int[this.size][this.size];

            for (int i = 0; i < this.size; i++)
                for (int j = 0; j < this.size; j++)
                    ret[i][j] = this.image[this.size - j - 1][i];

            this.image = ret;
            int topSav = this.top;
            this.top = this.left;
            this.left = this.bottom;
            this.bottom = this.right;
            this.right = topSav;
    }

    public void flip(){
        int[] temp;

        for (int i = 0; i < this.size / 2; i++){
            temp = this.image[this.size - i - 1];
            this.image[this.size - i - 1] = this.image[i];
            this.image[i] = temp;
        }
        int topSav = this.top;
        this.top = this.bottom;
        this.bottom = topSav;
    }

    public boolean isTopLeftCorner() {
        return this.top == 0 && this.left == 0;
    }

    public boolean isId(int id) {
        return this.id == id;
    }
}
