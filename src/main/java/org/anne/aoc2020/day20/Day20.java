package org.anne.aoc2020.day20;

import org.anne.util.File;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.lang.Math.sqrt;

public class Day20 {
    private static final String fileName = "Day20.txt";
    private static Map<Integer, Tile> tiles;

    public static void main(String[] args) {
        new Day20();
    }

    public Day20() {
        List<String> input = File.readFile(fileName);
        tiles = new HashMap<>();

        List<Integer> indexes = IntStream.range(0, input.size())
                .filter(i -> "".equals(input.get(i)))
                .boxed()
                .collect(Collectors.toList());
        indexes.add(input.size());

        int prev = -1;
        for (int index: indexes) {
            int id = Integer.parseInt(input.get(prev+1).replaceAll("^.+(\\d{4}).$", "$1"));
            Tile newTile = new Tile(id, new ArrayList<>(input.subList(prev+2, index)));
            tiles.put(id, newTile);
            prev = index;
        }

        System.out.println("The result for the multiplication of the ids of the four corners is: " + part1());
        System.out.println("The habitat's water roughness is: " + part2());
    }

    private long part1() {
        for (Map.Entry<Integer, Tile> first : tiles.entrySet()) {
            for (Map.Entry<Integer, Tile> second : tiles.entrySet()) {
                if (!first.getKey().equals(second.getKey())) {
                    if (compareImages(first.getValue(), second.getValue())) {
                        first.getValue().setTop(second.getKey());
                        second.getValue().setBottom(first.getKey());
                    }
                }
            }
        }
        return tiles.values().stream().filter(Tile::isCorner).mapToLong(Tile::getId).reduce(1, (a, b) -> a * b);
    }

    private long part2() {
        int nbTiles = (int) sqrt(tiles.size());
        int[][] canvas = new int[nbTiles][nbTiles];
        int id = tiles.values().stream().filter(Tile::isTopLeftCorner).mapToInt(Tile::getId).min().orElseThrow();
        for (int i = 0; i < nbTiles; i++) {
            canvas[i][0] = id;
            for (int j = 1; j < nbTiles; j++) {
                int rightId = tiles.get(id).getRight();
                int topId = i == 0 ? 0 : canvas[i-1][j];
                position(tiles.get(rightId), topId, id);
                id = rightId;
                canvas[i][j] = id;
            }
            if (i < nbTiles - 1) {
                int bottomId = tiles.get(canvas[i][0]).getBottom();
                position(tiles.get(bottomId), canvas[i][0], 0);
                id = bottomId;
            }
        }

        int borderlessTileSize = tiles.get(id).getImage()[0].length - 2;
        int imageSize = borderlessTileSize * nbTiles;
        int[][] assembledImage = new int[imageSize][imageSize];
        for (int i = 0; i < canvas.length; i++) {
            for (int j = 0; j < canvas.length; j++) {
                int[][] tile = tiles.get(canvas[i][j]).getBorderlessTile();
                for (int x = 0; x < borderlessTileSize; x++) {
                    System.arraycopy(tile[x], 0, assembledImage[i * borderlessTileSize + x], j * borderlessTileSize, borderlessTileSize);
                }
            }
        }
        Tile image = new Tile(assembledImage);
        purgeImageFromSeaMonster(image);

        return Arrays.stream(image.getImage())
                .flatMapToInt(Arrays::stream)
                .sum();
    }

    private void purgeImageFromSeaMonster(Tile image) {
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 4; j++) {
                if (findSeaMonster(image.getImage()) > 0) {
                    return;
                }
                image.rotate();
            }
            image.flip();
        }
    }

    private int findSeaMonster(int[][] image) {
        int count = 0;
        List<int[]> monster = Arrays.asList(new int[] {0,18},
                new int[] {1,0}, new int[] {1,5}, new int[] {1,6}, new int[] {1,11}, new int[] {1,12},
                new int[] {1,17}, new int[] {1,18}, new int[] {1,19},
                new int[] {2,1}, new int[] {2,4}, new int[] {2,7}, new int[] {2,10}, new int[] {2,13}, new int[] {2,16});
        for (int i = 0; i < image.length - 2; i++) {
            for (int j = 0; j < image.length - 19; j++) {
                boolean match = true;
                for (int[] monsterPart : monster) {
                    if (image[i+monsterPart[0]][j+monsterPart[1]] == 0) {
                        match = false;
                        break;
                    }
                }
                if (match) {
                    for (int[] monsterPart : monster) {
                        image[i+monsterPart[0]][j+monsterPart[1]] = 0;
                    }
                    count++;
                }
            }
        }
        return count;
    }

    private boolean compareImages(Tile first, Tile second) {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 2; j++) {
                if (Arrays.equals(first.topBorder(), second.bottomBorder())) {
                    return true;
                }
                first.rotate();
                if (Arrays.equals(first.topBorder(), second.bottomBorder())) {
                    return true;
                }
                first.rotate();
                if (Arrays.equals(first.topBorder(), second.bottomBorder())) {
                    return true;
                }
                first.rotate();
                if (Arrays.equals(first.topBorder(), second.bottomBorder())) {
                    return true;
                }
                first.flip();
            }
            second.rotate();
        }

        return false;
    }

    private void position(Tile t, int top, int left) {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (t.getTop() == top && t.getLeft() == left) {
                    return;
                }
                t.rotate();
                if (t.getTop() == top && t.getLeft() == left) {
                    return;
                }
                t.rotate();
                if (t.getTop() == top && t.getLeft() == left) {
                    return;
                }
                t.rotate();
                if (t.getTop() == top && t.getLeft() == left) {
                    return;
                }
                t.flip();
                if (t.getTop() == top && t.getLeft() == left) {
                    return;
                }
            }
        }
    }
}
