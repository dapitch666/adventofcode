package org.anne.aoc2020.day99;

import org.anne.util.File;

import java.util.List;

public class Day99 {
    private static final String fileName = "test.txt";

    public static void main(String[] args) {
        new Day99();
    }

    public Day99() {
        List<String> input = File.readFile(fileName);
        System.out.println("See ya tomorrow, pal!");
    }
}
