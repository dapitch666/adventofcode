package org.anne.aoc2020.day22;

import org.anne.util.File;

import java.util.ArrayList;
import java.util.List;

public class Day22 {
    private static final String fileName = "Day22.txt";

    public static void main(String[] args) {
        new Day22();
    }

    public Day22() {
        List<String> input = File.readFile(fileName);
        List<Integer> player1Deck = new ArrayList<>();
        List<Integer> player2Deck = new ArrayList<>();

        int index = input.indexOf("");
        for (int i = 1; i < input.size(); i++) {
            if (i < index) {
                player1Deck.add(Integer.valueOf(input.get(i)));
            } else if (i > index + 1) {
                player2Deck.add(Integer.valueOf(input.get(i)));
            }
        }
        List<Integer> player1 = new ArrayList<>(player1Deck);
        List<Integer> player2 = new ArrayList<>(player2Deck);
        System.out.println("The score for the classic game is " + classicGame(player1, player2) + " points");
        System.out.println("The score for the recursive game is " + recursiveCombat(player1Deck, player2Deck, true) + " points");
    }

    private int calculateScore(List<Integer> cards) {
        int score = 0;
        for (int i = 0; i < cards.size(); i++) {
            score += (cards.size() - i) * cards.get(i);
        }
        return score;
    }

    private int recursiveCombat(List<Integer> player1, List<Integer> player2, boolean isFirstGame) {
        int winner;
        List<List<Integer>> player1History = new ArrayList<>();
        List<List<Integer>> player2History = new ArrayList<>();
        while (player1.size() > 0 && player2.size() > 0) {
            if (player1History.contains(player1) && player2History.contains(player2)) {
                return 1;
            } else {
                player1History.add(new ArrayList<>(player1));
                player2History.add(new ArrayList<>(player2));
                int first = player1.get(0);
                int second = player2.get(0);
                player1.remove(0);
                player2.remove(0);
                if (player1.size() >= first && player2.size() >= second) {
                    winner = recursiveCombat(new ArrayList<>(player1.subList(0, first)), new ArrayList<>(player2.subList(0, second)), false);
                    if (winner == 1) {
                        player1.add(first);
                        player1.add(second);
                    } else {
                        player2.add(second);
                        player2.add(first);
                    }
                } else if (first < second) {
                    player2.add(second);
                    player2.add(first);
                } else {
                    player1.add(first);
                    player1.add(second);
                }
            }
        }
        if (isFirstGame) {
            if (player1.size() > 0) {
                return calculateScore(player1);
            } else {
                return calculateScore(player2);
            }
        } else {
            if (player1.size() > 0) {
                return 1;
            } else {
                return 2;
            }
        }
    }

    private int classicGame(List<Integer> player1, List<Integer> player2) {
        while (player1.size() > 0 && player2.size() > 0) {
            int first = player1.get(0);
            int second = player2.get(0);
            player1.remove(0);
            player2.remove(0);
            if (first < second) {
                player2.add(second);
                player2.add(first);
            } else {
                player1.add(first);
                player1.add(second);
            }
        }
        if (player1.size() > 0) {
            return calculateScore(player1);
        } else {
            return calculateScore(player2);
        }
    }
}
