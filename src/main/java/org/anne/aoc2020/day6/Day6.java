package org.anne.aoc2020.day6;

import org.anne.util.File;

import java.util.*;
import java.util.stream.Stream;

public class Day6 {
    private static final String fileName = "Day6.txt";

    public static void main(String[] args) {
        new Day6();
    }

    public Day6() {
        List<String> lines = File.readFile(fileName);
        lines.add(""); // Adding an empty line at the end of the list
        List<String> tempStr = new ArrayList<>();
        long sumPart1 = 0, sumPart2 = 0;
        for (String line : lines) {
            if (line.trim().isEmpty()) {
                sumPart1 += countDistinctChar(tempStr);
                sumPart2 += countAnswers(tempStr);
                tempStr.clear();
            } else {
                tempStr.add(line);
            }
        }
        System.out.println("Part1: " + sumPart1);
        System.out.println("Part2: " + sumPart2);
    }

    private long countDistinctChar(List<String> input) {
        Stream<Character> chars = String.join("", input).chars().mapToObj(c -> (char)c);
        return chars.distinct().count();
    }

    private long countAnswers(List<String> input) {
        Map<Character, Integer> charCount = characterCount(String.join("", input));
        return Collections.frequency(charCount.values(), input.size());
    }

    private Map<Character, Integer> characterCount(String str) {
        Map<Character, Integer> freqs = new HashMap<>();
        for (char c : str.toCharArray()) {
            freqs.merge(c,1, Integer::sum);
        }
        return freqs;
    }


}
