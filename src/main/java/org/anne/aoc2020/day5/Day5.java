package org.anne.aoc2020.day5;

import org.anne.util.File;

import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

public class Day5 {
    private static final String fileName = "day5.txt";

    public static void main(String[] args) {
        new Day5();
    }

    public Day5() {
        List<String> input = File.readFile(fileName);
        SortedSet<Integer> boardingPasses = new TreeSet<>();
        for (String ticket : input) {
            int row = decodeTicket(ticket, true);
            int column = decodeTicket(ticket, false);
            boardingPasses.add(row * 8 + column);
        }
        System.out.println("The highest seat ID on a boarding pass is " + boardingPasses.last());
        int i = boardingPasses.first();
        for (int seat : boardingPasses) {
            if (seat != i) {
                break;
            }
            i++;
        }
        System.out.println("The ID of my seat is " + i);
    }

    private int decodeTicket(String ticket, boolean isRow) {
        int min = 0;
        int max;
        char up;
        char down;
        if (isRow) {
            max = 127;
            up = 'F';
            down = 'B';
        } else {
            max = 7;
            up = 'L';
            down = 'R';
        }
        for (char c : ticket.toCharArray()) {
            int boundary =  ((max - min + 1) / 2) + min;
            if (c == up) {
                max = boundary - 1;
            } else if (c == down){
                min = boundary;
            }
        }
        return min;
    }
}
