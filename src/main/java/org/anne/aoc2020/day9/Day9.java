package org.anne.aoc2020.day9;

import org.anne.util.File;

import java.util.*;
import java.util.stream.Collectors;

public class Day9 {
    private static final String fileName = "Day9.txt";
    private static final int STEP = 25;
    public static void main(String[] args) {
        new Day9();
    }

    public Day9() {
        List<String> input = File.readFile(fileName);
        List<Long> longList = input.stream().map(Long::parseLong).collect(Collectors.toList());
        long weakNumber = findWeakness(longList, STEP);
        SortedSet<Long> encryptionWeakness = findEncryptionWeakness(longList, weakNumber);
        long result = encryptionWeakness.first() + encryptionWeakness.last();
        System.out.println("The first number which is not the sum of two of the " + STEP + " numbers before it is " + weakNumber);
        System.out.println("The encryption weakness is " + result);
    }

    private SortedSet<Long> findEncryptionWeakness(List<Long> longList, long weakNumber) {
        SortedSet<Long> resultSet = new TreeSet<>();
        for (int i = 0; i < longList.size(); i++) {
            long sum = longList.get(i);
            resultSet.add(sum);
            int j = i+1;
            while (sum < weakNumber) {
                long l = longList.get(j);
                resultSet.add(l);
                sum += l;
                j++;
            }
            if (sum == weakNumber) {
                return resultSet;
            } else {
                resultSet.clear();
            }
        }
        return resultSet;
    }

    private Long findWeakness(List<Long> longList, int step) {
        for (int i = step ; i < longList.size(); i++) {
            List<Long> entryList = longList.stream().skip(i-step).limit(step).collect(Collectors.toList());
            long sum = longList.get(i);
            if (!validateSum(entryList, sum)) {
                return sum;
            }
        }
        return 0L;
    }

    private boolean validateSum(List<Long> entryList, long sum) {
        while (entryList.size() > 0) {
            long entry = entryList.get(0);
            entryList.remove(0);
            for (long i : entryList) {
                if (entry + i == sum) {
                    return true;
                }
            }
        }
        return false;
    }
}
