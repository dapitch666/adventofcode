package org.anne.aoc2020.day11;

import org.anne.util.File;

import java.util.List;

public class Day11 {
    private static final String fileName = "Day11.txt";

    public static void main(String[] args) {
        new Day11();
    }

    public Day11() {
        List<String> input = File.readFile(fileName);

        char[][] waitingArea = input.stream().map(String::toCharArray).toArray(char[][]::new);

        System.out.println("Part 1: " + countSeats(waitingArea, true, 4) + " seats are occupied.");
        System.out.println("Part 2: " + countSeats(waitingArea, false, 5) + " seats are occupied.");
    }

    private int countSeats(char[][] waitingArea, boolean adjacent, int maxSeat) {
        int count = 0;
        boolean hasWaitingAreaChanged = true;
        while (hasWaitingAreaChanged) {
            hasWaitingAreaChanged = false;
            count = 0;
            char[][] waitingAreaCopy = new char[waitingArea.length][waitingArea[0].length];
            for (int i = 0; i < waitingArea.length; i++) {
                for (int j = 0; j < waitingArea[i].length; j++) {
                    char currentSeat = waitingArea[i][j];
                    if (currentSeat == '.') {
                        waitingAreaCopy[i][j] = currentSeat;
                    } else {
                        int adjacentSeatsCount;
                        if (adjacent) {
                            adjacentSeatsCount = countOccupiedAdjacentSeats(waitingArea, i, j);
                        } else {
                            adjacentSeatsCount = countOccupiedSeats(waitingArea, i, j);
                        }
                        if (currentSeat == 'L') {
                            if (adjacentSeatsCount == 0) {
                                waitingAreaCopy[i][j] = '#';
                                count++;
                                hasWaitingAreaChanged = true;
                            } else {
                                waitingAreaCopy[i][j] = 'L';
                            }
                        } else if (currentSeat == '#') {
                            if (adjacentSeatsCount > maxSeat) {
                                waitingAreaCopy[i][j] = 'L';
                                hasWaitingAreaChanged = true;
                            } else {
                                waitingAreaCopy[i][j] = '#';
                                count++;
                            }
                        }
                    }
//                    System.out.print(waitingAreaCopy[i][j]);
                }
//                System.out.println();
            }
            waitingArea = waitingAreaCopy;
        }
        return count;
    }

    private int countOccupiedAdjacentSeats(char[][] waitingArea, int i, int j) {
        int count = 0;
        for (int n = -1; n < 2; n++) {
            for (int m = -1; m < 2; m++) {
                try {
                    char s = waitingArea[i + n][j + m];
                    if (s == '#') {
                        count++;
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                    // Do nothing
                }
            }
        }
        return count;
    }

    private int countOccupiedSeats(char[][] waitingArea, int i, int j) {
        int count = 0;
        for (int n = -1; n < 2; n++) {
            for (int m = -1; m < 2; m++) {
                boolean isFloor = true;
                int mult = 1;
                while (isFloor) {
                    try {
                        char s = waitingArea[i + mult * n][j + mult * m];
                        if (s == 'L') {
                            isFloor = false;
                        }
                        if (s == '#') {
                            count++;
                            isFloor = false;
                        }
                    } catch (ArrayIndexOutOfBoundsException e) {
                        isFloor = false;
                    }
                    mult++;
                }
            }
        }
        return count;
    }
}
