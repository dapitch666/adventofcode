package org.anne.aoc2020.day17;

public class Cube {
    private final int x;
    private final int y;
    private final int z;
    private boolean active;

    public Cube(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Cube(int x, int y, int z, boolean active) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }

    public void activate() {
        this.active = true;
    }

    public void deactivate() {
        this.active = false;
    }
}

