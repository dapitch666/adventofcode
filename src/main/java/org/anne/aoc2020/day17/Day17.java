package org.anne.aoc2020.day17;

import org.anne.util.File;

import java.util.*;


public class Day17 {
    private static final String fileName = "Day17.txt";
    private static final int TURNS = 6;
    private final int finalSize;
    private final int size;


    public static void main(String[] args) {
        new Day17();
    }

    public Day17() {
        List<String> input = File.readFile(fileName);
        size = input.get(0).length();
        finalSize = size + 2 * TURNS;

        System.out.println("Part 1: " + part1(input) + " cubes are active.");
        System.out.println("Part 1: " + part2(input) + " cubes are active.");

    }

    private long part1(List<String> input) {
        int[][][] cubes = newCube();

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (input.get(i).charAt(j) == '#') {
                    cubes[TURNS][i + TURNS][j + TURNS] = 1;
                }
            }
        }

        for (int i = 0; i < TURNS; i++) {
            int[][][] cubesCopy = newCube();
            for (int x = 0; x < finalSize; x++) {
                for (int y = 0; y < finalSize; y++) {
                    for (int z = 0; z < finalSize; z++) {
                        int activeNeighbors = countActiveNeighbors(cubes, x, y, z);
                        if (cubes[x][y][z] == 1) {
                            if (activeNeighbors == 2 || activeNeighbors == 3) {
                                cubesCopy[x][y][z] = 1;
                            }
                        } else {
                            if (activeNeighbors == 3) {
                                cubesCopy[x][y][z] = 1;
                            }
                        }
                    }
                }
            }
            cubes = cubesCopy;
        }

        return Arrays.stream(cubes)
                .flatMap(Arrays::stream)
                .flatMapToInt(Arrays::stream)
                .sum();
    }

    private long part2(List<String> input) {
        int[][][][] cubes = newCubePart2();

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (input.get(i).charAt(j) == '#') {
                    cubes[TURNS][TURNS][i + TURNS][j + TURNS] = 1;
                }
            }
        }

        for (int i = 0; i < TURNS; i++) {
            int[][][][] cubesCopy = newCubePart2();
            for (int x = 0; x < finalSize; x++) {
                for (int y = 0; y < finalSize; y++) {
                    for (int z = 0; z < finalSize; z++) {
                        for (int w = 0; w < finalSize; w++) {
                            int activeNeighbors = countActiveNeighborsPart2(cubes, x, y, z, w);
                            if (cubes[x][y][z][w] == 1) {
                                if (activeNeighbors == 2 || activeNeighbors == 3) {
                                    cubesCopy[x][y][z][w] = 1;
                                }
                            } else {
                                if (activeNeighbors == 3) {
                                    cubesCopy[x][y][z][w] = 1;
                                }
                            }
                        }
                    }
                }
            }
            cubes = cubesCopy;
        }

        return Arrays.stream(cubes)
                .flatMap(Arrays::stream)
                .flatMap(Arrays::stream)
                .flatMapToInt(Arrays::stream)
                .sum();
    }

    private int countActiveNeighborsPart2(int[][][][] cube, int x, int y, int z, int w) {
        int count = 0;
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                for (int k = -1; k <= 1; k++) {
                    for (int l = -1; l <= 1; l++) {
                        if (i == 0 && j == 0 && k == 0 && l == 0) {
                            continue;
                        }
                        try {
                            count += cube[x + i][y + j][z + k][w + l];
                        } catch (ArrayIndexOutOfBoundsException e) {
                            // Do nothing
                        }
                    }
                }
            }
        }
        return count;
    }

    private int countActiveNeighbors(int[][][] cube, int x, int y, int z) {
        int count = 0;
        for (int i = -1; i <= 1; i++) { // Layer
            for (int j = -1; j <= 1; j++) { // Row
                for (int k = -1; k <= 1; k++) { // Char
                    if (i == 0 && j == 0 && k == 0) {
                        continue;
                    }
                    try {
                        count += cube[x+i][y+j][z+k];
                    } catch (ArrayIndexOutOfBoundsException e) {
                        // Do nothing
                    }
                }
            }
        }
        return count;
    }

    private int[][][] newCube() {
        int[][][] cube = new int[finalSize][finalSize][finalSize];
        for (int i = 0; i < finalSize; i++) {
            for (int j = 0; j < finalSize; j++) {
                for (int k = 0; k < finalSize; k++) {
                    cube[i][j][k] = 0;
                }
            }
        }
        return cube;
    }

    private int[][][][] newCubePart2() {
        int[][][][] cube = new int[finalSize][finalSize][finalSize][finalSize];
        for (int i = 0; i < finalSize; i++) {
            for (int j = 0; j < finalSize; j++) {
                for (int k = 0; k < finalSize; k++) {
                    for (int l = 0; l < finalSize; l++) {
                        cube[i][j][k][l] = 0;
                    }
                }
            }
        }
        return cube;
    }
}
