package org.anne.aoc2020.day25;

import org.anne.util.File;

import java.util.List;

public class Day25 {

    private static final int DIVIDER = 20201227;
    public static void main(String[] args) {
        new Day25();
    }

    public Day25() {
        int cardPublicKey = 13316116;
        int doorPublicKey = 13651422;
        int doorLoopSize = getLoopSize(doorPublicKey);
        System.out.println("The encryption key is " + transform(cardPublicKey, doorLoopSize));
    }

    private int getLoopSize(int key) {
        int count = 0;
        int result = 1;
        while (result != key) {
            count++;
            result *= 7;
            result %= DIVIDER;
        }
        return count;
    }

    private long transform (int subjectNumber, int loopSize) {
        long result = 1;
        for (int i = 0; i < loopSize; i++) {
            result *= subjectNumber;
            result %= DIVIDER;
        }
        return result;
    }
}
