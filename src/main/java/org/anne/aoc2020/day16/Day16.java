package org.anne.aoc2020.day16;

import org.anne.util.File;

import java.util.*;

public class Day16 {
    private static final String fileName = "Day16.txt";
    private final List<Interval> intervals = new ArrayList<>();


    public static void main(String[] args) {
        new Day16();
    }

    public Day16() {
        List<String> input = File.readFile(fileName);
        List<String> rules = new ArrayList<>();
        List<int[]> tickets = new ArrayList<>();
        List<Field> fields = new ArrayList<>();
        Set<int[]> validTickets = new HashSet<>();
        int firstEmptyLineIdx = input.indexOf("");
        int lastEmptyLineIdx = input.lastIndexOf("");
        for (int i = 0; i < firstEmptyLineIdx; i++) {
            rules.add(input.get(i));
        }
        int[] myTicket = parseTicket(input.get(firstEmptyLineIdx + 2));
        for (int i = lastEmptyLineIdx + 2; i < input.size(); i++) {
            tickets.add(parseTicket(input.get(i)));
        }
        for (String line: rules) {
            Field field = new Field(line);
            fields.add(field);
            intervals.addAll(field.getIntervals());
        }
        int count = 0;
        for (int[] ticket : tickets) {
            boolean valid = true;
            for (int i : ticket) {
                if (!isValid(i)) {
                    count += i;
                    valid = false;
                }
            }
            if (valid) {
                validTickets.add(ticket);
            }
        }
        System.out.println("The ticket scanning error rate is " + count);

        validTickets.add(myTicket);
        Field[] orderedFields = new Field[myTicket.length];
        for (int i = 0; i < myTicket.length; i++) {
            List<Integer> integerList = new ArrayList<>();
            for (int[] t : validTickets) {
                integerList.add(t[i]);
            }
            for (Field field : fields) {
                if (field.areValid(integerList)) {
                    field.addOrder(i);
                }
            }
        }

        for (int i = 0; i < orderedFields.length; i++) {
            for (Field field : fields) {
                if (field.countOrders() == 1) {
                    int currentOrder = field.getOrder();
                    orderedFields[currentOrder] = field;
                    for (Field f : fields) {
                        f.removeOrder(currentOrder);
                    }
                    break;
                }
            }
        }
        long myTicketDeparture = 1;
        int i = 0;
        for (Field field : orderedFields) {
            if (field.startsWithDeparture()) {
                myTicketDeparture *= myTicket[i];
            }
            i++;
        }
        System.out.println(myTicketDeparture);
    }

    private boolean isValid(int i) {
        for (Interval interval : intervals) {
            if (interval.validate(i)) {
                return true;
            }
        }
        return false;
    }

    private int[] parseTicket(String line) {
        return Arrays.stream(line.split(",")).mapToInt(Integer::parseInt).toArray();
    }
}
