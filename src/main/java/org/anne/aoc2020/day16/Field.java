package org.anne.aoc2020.day16;


import java.util.*;

public class Field {
    private final String name;
    private final Interval interval1;
    private final Interval interval2;
    private Set<Integer> orders = new HashSet<>();

    public Field(String s) {
        String[] sSplit = s.split(": ");
        this.name = sSplit[0];
        this.interval1 = new Interval(sSplit[1].split(" or ")[0]);
        this.interval2 = new Interval(sSplit[1].split(" or ")[1]);
    }

    public boolean startsWithDeparture() {
        return name.matches("departure(.*)");
    }

    public List<Interval> getIntervals() {
        return Arrays.asList(interval1, interval2);
    }

    public boolean areValid(List<Integer> ints) {
        for (int j : ints) {
            if (!interval1.validate(j) && !interval2.validate(j)) {
                return false;
            }
        }
        return true;
    }

    public void addOrder(int order) {
        this.orders.add(order);
    }

    public void removeOrder(int order) {
        this.orders.remove(order);
    }

    public boolean hasOrder(int order) {
        return this.orders.contains(order);
    }

    public int countOrders() {
        return this.orders.size();
    }

    public int getOrder() {
        return this.orders.iterator().next();
    }
}
