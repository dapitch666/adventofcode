package org.anne.aoc2020.day16;

public class Interval {
    private final int min;
    private final int max;

    public Interval(String s) {
        String[] sSplit = s.split("-");
        this.min = Integer.parseInt(sSplit[0]);
        this.max = Integer.parseInt(sSplit[1]);
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }

    public boolean validate(int i) {
        return i >= min && i <= max;
    }
}
