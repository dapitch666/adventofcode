package org.anne.aoc2020.day7;

import org.anne.util.File;

import java.util.*;
import java.util.stream.Collectors;

public class Day7 {
    private static final String fileName = "Day7.txt";
    private static final String MYBAG = "shiny gold";
    public static void main(String[] args) {
        new Day7();
    }

    public Day7() {
        List<String> input = File.readFile(fileName);
        Map<String, Map<String, Integer>> bags = decode(input);
//        bags.forEach((key, value) -> System.out.println(key + ":" + value));
        Set<String> okBags = new TreeSet<>();
        Set<String> okBagsTmp = new TreeSet<>();
        boolean newBagsAdded = okBags.addAll(getBags(bags, MYBAG));
        while (newBagsAdded) {
            for (String b : okBags) {
                okBagsTmp.addAll(getBags(bags, b));
            }
            newBagsAdded = okBags.addAll(okBagsTmp);
            okBagsTmp.clear();
        }
        System.out.println(okBags.size() + " bag colors can contain at least one shiny gold bag.");

        int nbBags = getNbBags(MYBAG, bags);
        System.out.println(nbBags + " are required inside my single shiny gold bag.");

    }

    private int getNbBags(String color, Map<String, Map<String, Integer>> bags) {
        int count = 0;
        if(!bags.containsKey(color)) {
            return 0;
        }
        for (Map.Entry<String, Integer> entry : bags.get(color).entrySet()) {
            String bagColor = entry.getKey();
            int nbBags = entry.getValue();
            count += nbBags + nbBags * getNbBags(bagColor, bags);
        }
        return count;
    }

    private Set<String> getBags(Map<String, Map<String, Integer>> bags, String bagColor) {
        Set<String> bagsSet = new TreeSet<>();
        bags.forEach((key, value) -> {
            if (value.containsKey(bagColor)) {
                bagsSet.add(key);
            }
        });
        return bagsSet;
    }

    private Map<String, Map<String, Integer>> decode(List<String> input) {
        Map<String, Map<String, Integer>> bags = new HashMap<>();
        for (String line : input) {
            String[] lineSplit = line.split(" bags contain ");
            String[] lineSplitSplit = lineSplit[1].split(", ");
            //System.out.println(lineSplit[0]);
            Map<String, Integer> bagList = Arrays.stream(lineSplitSplit)
                    .filter(s -> !s.equals("no other bags."))
                    .map(this::color)
                    .collect(Collectors.toMap(e -> e[1], e -> Integer.parseInt(e[0])));
            bags.put(lineSplit[0], bagList);
        }
        return bags;
    }

    private String[] color(String s) {
        return new String[]{s.substring(0, 1), s.replaceFirst("^\\d (\\w+ \\w+) .*$", "$1")};
    }
}
