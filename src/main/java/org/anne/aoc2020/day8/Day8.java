package org.anne.aoc2020.day8;

import org.anne.util.File;

import java.util.List;
import java.util.stream.Collectors;

public class Day8 {
    private static final String fileName = "Day8.txt";
    private int accumulator;

    public static void main(String[] args) {
        new Day8();
    }

    public Day8() {
        List<String> input = File.readFile(fileName);
        List<Instruction> bootCode = input.stream().map(Instruction::new).collect(Collectors.toList());
        accumulator = 0;
        run(bootCode, -1L);
        System.out.println("Part1: The value in the accumulator before infinite loop is " + accumulator);
        long permutations = bootCode.stream().filter(i -> !i.getOperation().equals("acc")).count();

        for (long i = 0L; i < permutations; i++) {
            accumulator = 0;
            bootCode.forEach(Instruction::init);
            if (run(bootCode, i)) {
                break;
            }
        }
        System.out.println("Part2: The value in the accumulator after the program terminates is " + accumulator);
    }

    private boolean run(List<Instruction> bootCode, long change) {
        int i = 0;
        int countJmpNop = 0;
        while (i < bootCode.size()) {
            Instruction instruction = bootCode.get(i);
            if (instruction.isUsed()) {
                return false;
            }
            instruction.use();
            String operation = instruction.getOperation();
            if (!operation.equals("acc")) {
                countJmpNop++;
                if (countJmpNop == change) {
                    operation = operation.equals("jmp") ? "nop" : "jmp";
                }
            }
            switch (operation) {
                case "acc":
                    accumulator += instruction.getArgument();
                    i++;
                    break;
                case "jmp":
                    i += instruction.getArgument();
                    break;
                case "nop":
                    i++;
                    break;
            }
        }
        return true;
    }

    public static class Instruction {
        private final String operation;
        private final int argument;
        private boolean used;

        public Instruction(String line) {
            String[] lineArray = line.split(" ");
            this.operation = lineArray[0];
            this.argument = Integer.parseInt(lineArray[1]);
            this.used = false;
        }

        public String getOperation() {
            return operation;
        }

        public int getArgument() {
            return argument;
        }

        public boolean isUsed() {
            return used;
        }

        public void use() {
            this.used = true;
        }

        public void init() {
            this.used = false;
        }
    }
}
