package org.anne.aoc2020.day10;

import org.anne.util.File;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Day10 {
    private static final String fileName = "Day10.txt";

    public static void main(String[] args) {
        new Day10();
    }

    public Day10() {
        List<String> input = File.readFile(fileName);
        input.add("0");
        List<Integer> adapters = input.stream()
                .map(Integer::parseInt)
                .sorted()
                .collect(Collectors.toList());

        int one = 0, three = 1, previousAdapter = 0;
        for (int adapter : adapters) {
            int joltDiff = adapter - previousAdapter;
            if (joltDiff == 1) {
                one++;
            } else if (joltDiff == 3) {
                three++;
            }
            previousAdapter = adapter;
        }
        System.out.println("There are " + one + " 1-jolt differences and "
                + three + " 3-jolt differences, so the answer is: " + one * three);

        adapters.add(adapters.get(adapters.size() - 1) + 3);
        List<Integer> tmpList = new ArrayList<>();
        tmpList.add(0);
        double count = 1;
        for (int i = 0; i < adapters.size() - 1; i++) {
            int currentAdapter = adapters.get(i);
            int nextAdapter = adapters.get(i + 1);
            if (nextAdapter - currentAdapter != 1) {
                int mult = 1;
                switch (tmpList.size()) {
                    case 0:
                    case 1:
                    case 2:
                        break;
                    case 3:
                        mult = 2;
                        break;
                    case 4:
                        mult = 4;
                        break;
                    case 5:
                        mult = 7;
                        break;
                    default:
                        System.out.println("This program does not work for more than 5 consecutive values and we have " + tmpList.size());
                }
                count = count * mult;
                tmpList.clear();
            }
            tmpList.add(nextAdapter);
        }
        System.out.println("Adapters can be arranged in " + (long) count + " different ways");
    }
}
