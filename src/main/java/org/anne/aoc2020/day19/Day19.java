package org.anne.aoc2020.day19;

import org.anne.util.File;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Day19 {
    private static final String fileName = "Day19.txt";

    public static void main(String[] args) {
        new Day19();
    }

    public Day19() {
        List<String> input = File.readFile(fileName);
        int SepIdx = input.indexOf("");
        Map<Integer, String> rules = input.subList(0, SepIdx).stream()
                .map(s -> s.split(": "))
                .collect(Collectors.toMap(a -> Integer.parseInt(a[0]), a -> a[1].replaceAll("\"", "")));
        List<String> messages = input.subList(SepIdx+1, input.size());

        System.out.println("Part1: " + countValidMessages(rules, messages) + " messages match rule 0");

        rules = input.subList(0, SepIdx).stream()
                .map(s -> s.split(": "))
                .collect(Collectors.toMap(a -> Integer.parseInt(a[0]), a -> a[1]));
        rules.put(8, "42 | 42 8");
        rules.put(11, "42 31 | 42 11 31");
        System.out.println("Part2: " + countValidMessages(rules, messages) + " messages match rule 0");
    }

    public int countValidMessages(Map<Integer, String> rules, List<String> messages) {
        for (Map.Entry<Integer, String> rule : rules.entrySet()) {
            rule.setValue(getRule(rule.getValue(), rules, 0));
        }
        int count = 0;
        for (String message : messages) {
            if (message.matches(rules.get(0))) {
                count++;
            }
        }
        return count;
    }

    public String getRule(String rule, Map<Integer, String> rules, int counter) {
        if (counter > 30) {
            return "";
        }
        String[] ruleSplit = rule.split(" ");
        for (int i = 0; i < ruleSplit.length; ++i) {
            if (ruleSplit[i].matches("\\d+")) {
                String ruleResult = getRule(rules.get(Integer.parseInt(ruleSplit[i])), rules, counter++);
                if (ruleResult.length() > 1) {
                    ruleSplit[i] = "(" + ruleResult + ")";
                } else {
                    ruleSplit[i] = ruleResult;
                }
            }
        }
        return String.join("", ruleSplit).replaceAll("\"", "");
    }
}
