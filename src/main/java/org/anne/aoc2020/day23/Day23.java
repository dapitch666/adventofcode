package org.anne.aoc2020.day23;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Day23 {
    public static void main(String[] args) {
        new Day23();
    }

    public Day23() {

        System.out.println("The order after cup 1 is " + String.join("", part1()));
//        System.out.println("The labels of the two cups multiplied together is " + part2());
    }

    private long part2() {
        List<Integer> circle = new ArrayList<>(Arrays.asList(2,1,9,7,4,8,3,6,5));
        // List<Integer> circle = new ArrayList<>(Arrays.asList(3,8,9,1,2,5,4,6,7));
        for (int i = 10; i <= 1000000; i++) {
            circle.add(i);
        }


        int curCup = circle.get(0);
        for (int i = 0; i < 10000000; i++) {
            int curIndex = circle.indexOf(curCup);
            List<Integer> threeCups = new ArrayList<>();
            for (int j = 0; j < 3; j++) {
                threeCups.add(circle.get(curIndex + 1));
                circle.remove(curIndex + 1);
            }

            int destCup = curCup - 1;
            while (!circle.contains(destCup)) {
                if (destCup < 1) {
                    destCup = 1000000;
                } else {
                    destCup--;
                }
            }
            int destIndex = circle.indexOf(destCup);
            circle.add(destIndex + 1, threeCups.get(0));
            circle.add(destIndex + 2, threeCups.get(1));
            circle.add(destIndex + 3, threeCups.get(2));
            curIndex = circle.indexOf(curCup);
            curCup = circle.get(curIndex + 1);

            if (curIndex > 999990) {
                List<Integer> tmpCircle = new ArrayList<>();
                for (int j = circle.indexOf(curCup); j < circle.size(); j++) {
                    tmpCircle.add(circle.get(j));
                }
                for (int j = 0; j < circle.indexOf(curCup); j++) {
                    tmpCircle.add(circle.get(j));
                }
                circle = tmpCircle;
            }
        }

        return (long) circle.get(circle.indexOf(1) + 1) * circle.get(circle.indexOf(1) + 2);
    }

    private List<String> part1() {
         List<Integer> circle = new ArrayList<>(Arrays.asList(2,1,9,7,4,8,3,6,5));
        //List<Integer> circle = new ArrayList<>(Arrays.asList(3,8,9,1,2,5,4,6,7));

        int curCup = circle.get(0);
        for (int i = 0; i < 100; i++) {
            /*System.out.println("-- move " + (i+1) + " --");
            System.out.println("cups: " + circle.toString());
            System.out.println("Cur cup:" + curCup);*/
            int curIndex = circle.indexOf(curCup);
            List<Integer> threeCups = new ArrayList<>();
            for (int j = 0; j < 3; j++) {
                threeCups.add(circle.get(curIndex + 1));
                circle.remove(curIndex + 1);
            }
            // System.out.println("pick up: " + threeCups.toString());

            int destCup = curCup - 1;
            while (!circle.contains(destCup)) {
                if (destCup < 1) {
                    destCup = 9;
                } else {
                    destCup--;
                }
            }
            // System.out.println("destination: " + destCup);
            int destIndex = circle.indexOf(destCup);
            circle.add(destIndex + 1, threeCups.get(0));
            circle.add(destIndex + 2, threeCups.get(1));
            circle.add(destIndex + 3, threeCups.get(2));

            curCup = circle.get(circle.indexOf(curCup) + 1);
            List<Integer> tmpCircle = new ArrayList<>();
            for (int j = circle.indexOf(curCup); j < circle.size(); j++) {
                tmpCircle.add(circle.get(j));
            }
            for (int j = 0; j < circle.indexOf(curCup); j++) {
                tmpCircle.add(circle.get(j));
            }
            circle = tmpCircle;
        }
        List<String> finalCircle = new ArrayList<>();
        for (int j = circle.indexOf(1) + 1; j < circle.size(); j++) {
            finalCircle.add(String.valueOf(circle.get(j)));
        }
        for (int j = 0; j < circle.indexOf(1); j++) {
            finalCircle.add(String.valueOf(circle.get(j)));
        }
        return finalCircle;
    }
}
