package org.anne.aoc2020.day13;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day13 {
    private static final int myTime = 1007268;
    private static final String input = "17,x,x,x,x,x,x,41,x,x,x,x,x,x,x,x,x,937,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,13,x,x,x,x,23,x,x,x,x,x,29,x,397,x,x,x,x,x,37,x,x,x,x,x,x,x,x,x,x,x,x,19";

    public static void main(String[] args) {
        new Day13();
    }

    public Day13() {
        List<Integer> busses = Stream.of(input.split(","))
                .map(this::mapToInt)
                .collect(Collectors.toList());
        System.out.println("Part 1 result is: " + minWait(busses));
        System.out.println("Part 2 result is: " + minTime(busses));
    }

    private long minTime(List<Integer> busses) {
        long time = 0L;
        long multiple = 1L;
        for(int i = 0; i < busses.size(); i++) {
            int bus = busses.get(i);
            if (bus > 0) {
                while ((time + i) % bus != 0) {
                    time += multiple;
                }
                multiple *= bus;
            }
        }
        return time;
    }

    private int mapToInt(String s) {
        if (s.equals("x")) {
            return 0;
        } else {
            return Integer.parseInt(s);
        }
    }

    private int minWait(List<Integer> busses) {
        int curBus = 0;
        int minWait = Integer.MAX_VALUE;
        for (int bus : busses) {
            if (bus > 0) {
                int wait = (myTime / bus + 1) * bus - myTime;
                if (wait < minWait) {
                    minWait = wait;
                    curBus = bus;
                }
            }
        }
        return curBus * minWait;
    }

}
