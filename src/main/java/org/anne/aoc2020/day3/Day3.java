package org.anne.aoc2020.day3;

import org.anne.util.File;

import java.util.List;

public class Day3 {
    private static final String filename = "Day3.txt";
    private final List<String> input = File.readFile(filename);
    private final int lineLength = input.get(0).length();

    public static void main(String[] args) {
        new Day3();
    }

    public Day3() {
        int descente11 = descente(1, 1);
        int descente31 = descente(3, 1);
        int descente51 = descente(5, 1);
        int descente71 = descente(7, 1);
        int descente12 = descente(1, 2);
        System.out.println("Right 1 down 1: " + descente11 + " arbres");
        System.out.println("Right 3 down 1: " + descente31 + " arbres");
        System.out.println("Right 5 down 1: " + descente51 + " arbres");
        System.out.println("Right 7 down 1: " + descente71 + " arbres");
        System.out.println("Right 1 down 2: " + descente12 + " arbres");
        System.out.println("Total = " + descente11 * descente31 * descente51 * descente71 * descente12);
    }

    private int descente(int right, int down) {
        int arbres = 0;
        for (int i = down; i < input.size(); i += down) {
            int index = i * right / down;
            int mult = index / lineLength + 1;
            String line = input.get(i).trim().repeat(mult);
            if (line.charAt(index) == '#') {
                arbres++;
            }
        }
        return arbres;
    }
}
