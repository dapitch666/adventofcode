package org.anne.aoc2020.day21;

import org.anne.util.File;

import java.util.*;
import java.util.stream.Collectors;

public class Day21 {
    private static final String fileName = "Day21.txt";

    public static void main(String[] args) {
        new Day21();
    }

    public Day21() {
        List<String> input = File.readFile(fileName);
        Map<String, Integer> ingredientsOccurences = new HashMap<>();
        Map<String, Set<String>> allergensIngredients = new TreeMap<>();
        for (String line : input) {

            String[] split = line.split("\\(");
            List<String> ingredients = Arrays.stream(split[0].split(" ")).collect(Collectors.toList());
            List<String> allergens = Arrays.stream(split[1]
                    .replaceFirst("contains ", "")
                    .replaceFirst("\\)", "")
                    .split(", ")).collect(Collectors.toList());
            // System.out.println(ingredients + ", " + allergens);
            for (String ingredient : ingredients) {
                ingredientsOccurences.put(ingredient, ingredientsOccurences.getOrDefault(ingredient, 0) + 1);
            }
            for (String allergen : allergens) {
                Set<String> value;
                if (allergensIngredients.containsKey(allergen)) {
                    value = allergensIngredients.get(allergen).stream().filter(ingredients::contains).collect(Collectors.toSet());
                } else {
                    value = new HashSet<>(ingredients);
                }
                allergensIngredients.put(allergen, value);
            }
        }
        Set<String> badIngredients = new HashSet<>();
        for(Map.Entry<String, Set<String>> allergensIngredient : allergensIngredients.entrySet()) {
            badIngredients.addAll(allergensIngredient.getValue());
        }
        int count = 0;
        for (Map.Entry<String, Integer> occ : ingredientsOccurences.entrySet()) {
            if (!badIngredients.contains(occ.getKey())) {
                count += occ.getValue();
            }
        }
        // ingredientsMap.forEach((key, value) -> System.out.println(key + ":" + value));
        // allergensIngredients.forEach((key, value) -> System.out.println(key + ":" + value));

        System.out.println("The ingredients without any allergen appear " + count + " times.");

        while (!isDeduped(allergensIngredients)) {
            for (Set<String> ingredients : allergensIngredients.values()) {
                if (ingredients.size() == 1) {
                    for (String allergene : allergensIngredients.keySet()) {
                        if (allergensIngredients.get(allergene) != ingredients) {
                            allergensIngredients.get(allergene).remove(ingredients.iterator().next());
                        }
                    }
                }
            }
        }
        System.out.println(allergensIngredients.values().stream().map(String::valueOf)
                .collect(Collectors.joining(","))
                .replaceAll("]", "")
                .replaceAll("\\[", ""));

    }

    private boolean isDeduped (Map<String, Set<String>> map) {
        for (Map.Entry<String, Set<String>> mapEntry : map.entrySet()) {
            if (mapEntry.getValue().size() > 1) {
                return false;
            }
        }
        return true;
    }

}
