package org.anne.aoc2020.day4;

import org.anne.util.File;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Day4 {
    private static final String filename = "Day4.txt";

    public static void main(String[] args) {
        new Day4();
    }

    public Day4() {
        List<String> lines = File.readFile(filename);
        List<String> input = new ArrayList<>();
        List<String> tempStr = new ArrayList<>();
        for (String line : lines) {
            if (line.trim().isEmpty()) {
                input.add(String.join(" ", tempStr));
                tempStr.clear();
            } else {
                tempStr.add(line);
            }
        }
        input.add(String.join(" ", tempStr));
        System.out.println("Défi 1: " + defi1(input) + " passports are valid");
        System.out.println("Défi 2: " + defi2(input) + " passports are valid");
    }

    private long defi1(List<String> input) {
        return input.stream().filter(i -> parseInput(i).isValidDefi1()).count();
    }

    private long defi2(List<String> input) {
        return input.stream().filter(i -> parseInput(i).isValidDefi2()).count();
    }

    private static Passport parseInput(String input) {
        HashMap<String, String> inputMap = new HashMap<>();
        String[] keyVals = input.split(" ");
        for (String keyVal : keyVals) {
            String[] parts = keyVal.split(":",2);
            inputMap.put(parts[0], parts[1]);
        }
        return new Passport(inputMap.get("byr"), inputMap.get("iyr"), inputMap.get("eyr"), inputMap.get("hgt"),
                inputMap.get("hcl"), inputMap.get("ecl"), inputMap.get("pid"));
    }
}
