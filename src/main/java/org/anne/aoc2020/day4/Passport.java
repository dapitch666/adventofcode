package org.anne.aoc2020.day4;

import java.util.Arrays;
import java.util.List;

public class Passport {
    private final String birthYear;
    private final String issueYear;
    private final String expirationYear;
    private final String height;
    private final String hairColor;
    private final String eyeColor;
    private final String passportId;

    public Passport(String birthYear, String issueYear, String expirationYear, String height, String hairColor, String eyeColor, String passportId) {
        this.birthYear = birthYear;
        this.issueYear = issueYear;
        this.expirationYear = expirationYear;
        this.height = height;
        this.hairColor = hairColor;
        this.eyeColor = eyeColor;
        this.passportId = passportId;
    }

    public boolean isValidDefi1() {
        return birthYear != null && issueYear != null && expirationYear != null &&
               height != null && hairColor != null && eyeColor != null && passportId != null;
    }

    public boolean isValidDefi2() {
        if (isValidDefi1()) {
            return validYear(birthYear, 1920, 2002) &&
                    validYear(issueYear, 2010, 2020) &&
                    validYear(expirationYear, 2020, 2030) &&
                    validHgt(height) &&
                    validHcl(hairColor) &&
                    validEcl(eyeColor) &&
                    validPid(passportId);
        }
        return false;
    }

    private boolean validPid(String passportId) {
        return passportId.matches("^\\d{9}$");
    }

    private boolean validEcl(String eyeColor) {
        List<String> eyeColorList = Arrays.asList("amb", "blu", "brn", "gry", "grn", "hzl", "oth");
        return eyeColorList.contains(eyeColor);
    }

    private boolean validHcl(String hairColor) {
        return hairColor.matches("^#[0-9a-f]{6}$");
    }

    private boolean validHgt(String height) {
        String pattern = "^(\\d+)(in|cm)$";
        if (height.matches(pattern)) {
            int heightValue = Integer.parseInt(height.replaceAll(pattern, "$1"));
            String heightUnit = height.replaceAll(pattern, "$2");
            if (heightUnit.equals("in")) {
                return heightValue >= 59 && heightValue <= 76;
            } else {
                return heightValue >= 150 && heightValue <= 193;
            }
        }
        return false;
    }

    private boolean validYear(String yr, int min, int max) {
        if (yr.matches("\\d{4}")) {
            return Integer.parseInt(yr) >= min && Integer.parseInt(yr) <= max;
        }
        return false;
    }
}
