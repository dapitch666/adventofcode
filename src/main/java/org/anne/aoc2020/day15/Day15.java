package org.anne.aoc2020.day15;

import java.util.*;

public class Day15 {

    public static void main(String[] args) {
        new Day15();
    }

    public Day15() {
        List<Integer> input = Arrays.asList(14,3,1,0,9,5);

        System.out.println("The 2020th number spoken is " + getLastSpokenNumber(input, 2020));
        System.out.println("The 30000000th number spoken is " + getLastSpokenNumber(input, 30000000));
    }

    private int getLastSpokenNumber(List<Integer> input, int maxTurns) {
        int spokenNumber = 0;
        Map<Integer, Integer> spokenNumbers = new HashMap<>();
        for (int i = 0; i < input.size(); i++) {
            spokenNumbers.put(input.get(i), i + 1);
        }
        for (int i = input.size() + 1; i < maxTurns; i++) {
            if (spokenNumbers.containsKey(spokenNumber)) {
                int val = spokenNumbers.get(spokenNumber);
                spokenNumbers.put(spokenNumber, i);
                spokenNumber = i - val;
            } else {
                spokenNumbers.put(spokenNumber, i);
                spokenNumber = 0;
            }
        }
        return spokenNumber;
    }
}
