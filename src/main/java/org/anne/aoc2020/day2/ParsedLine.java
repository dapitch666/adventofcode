package org.anne.aoc2020.day2;

public class ParsedLine {
    private final String pwd;
    private final char letter;
    private final int min;
    private final int max;

    public ParsedLine(String pwd, char letter, int min, int max) {
        this.pwd = pwd;
        this.letter = letter;
        this.min = min;
        this.max = max;
    }

    public boolean isValidPart1() {
        long occurence = pwd.chars().filter(c -> c == letter).count();
        return occurence >= min && occurence <= max;
    }

    public boolean isValidPart2() {
        return (pwd.charAt(min-1) == letter) ^ (pwd.charAt(max-1) == letter);
    }
}
