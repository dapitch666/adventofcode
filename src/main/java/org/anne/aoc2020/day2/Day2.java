package org.anne.aoc2020.day2;

import org.anne.util.File;

import java.util.List;

public class Day2 {
    private static final String fileName = "Day2.txt";

    public static void main(String[] args) {
        new Day2();
    }

    public Day2() {
        List<String> input = File.readFile(fileName);
        System.out.println("Défi 1: " + defi1(input) + " passwords are valid");
        System.out.println("Défi 2: " + defi2(input) + " passwords are valid");
    }

    private long defi1(List<String> input) {
        return input.stream().filter(i -> parseInput(i).isValidPart1()).count();
    }

    private long defi2(List<String> input) {
        return input.stream().filter(i -> parseInput(i).isValidPart2()).count();
    }

    private ParsedLine parseInput(String input) {
        String[] x = input.split(":");
        String[] y = x[0].split(" ");
        String[] z = y[0].split("-");

        String pwd = x[1].trim();
        char letter = y[1].charAt(0);
        int min = Integer.parseInt(z[0]);
        int max = Integer.parseInt(z[1]);

        return new ParsedLine(pwd, letter, min, max);
    }

}

// 9-10 m: mmmmnxmmmwm
