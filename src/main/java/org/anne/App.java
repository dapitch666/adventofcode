package org.anne;

import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;

import java.lang.reflect.InvocationTargetException;
import java.util.Comparator;
import java.util.List;

public class App
{
    private static final String pattern = "(Day)(\\d+)";

    public static void main( String[] args ) {


        Reflections reflections = new Reflections("org.anne.aoc2020", new SubTypesScanner(false));
        List<Class<?>> allClasses = new java.util.ArrayList<>(List.copyOf(reflections.getSubTypesOf(Object.class)));

        allClasses.stream()
                .filter(clazz -> last(clazz.getCanonicalName().split("\\.")).matches(pattern))
                .sorted(Comparator.comparingInt(App::getDayNumber))
                .forEach(App::executeClazz);
    }

    private static void executeClazz(Class<?> clazz) {
        String last = last(clazz.getCanonicalName().split("\\."));
        System.out.println("=========");
        System.out.println("  " + last);
        System.out.println("=========");

        try {
            clazz.getDeclaredConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
        System.out.println();
    }

    public static <T> T last(T[] array) {
        return array[array.length - 1];
    }

    public static int getDayNumber(Class<?> clazz) {
        return Integer.parseInt(last(clazz.getCanonicalName().split("\\.")).replaceAll(pattern, "$2"));
    }
}
